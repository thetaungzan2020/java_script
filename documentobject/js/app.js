// // let val;
// // val = document;
// // val = document.doctype;
// // val = document.head;
// // val = document.body;
// // val = document.URL;

// // val = document.links;//HTMLCollection
// // val = document.links[0];
// // val = document.links[1];
// // val = document.links[1].id;
// // val = document.links[1].className;
// // val = document.links[0].className;
// // val = document.links[0].classList;//DOMTokenList
// // val = document.links[1].classList;
// // val = document.links[0].classList[1];

// // val = document.forms;//HTMLCollection
// // val = document.forms[0];
// // val = document.forms[0].action;
// // val = document.forms[0].method;

// // val = document.images;//HTMLCollection
// // val = document.images[0];
// // val = document.images[0].alt;
// // val = document.images[0].className;
// // val = document.images[0].src;
// // val = document.images[0].getAttribute('abc');


// // val = document.scripts;//HTMLCollection
// // val = document.scripts[2];
// // val = document.scripts[2].src;
// // val = document.scripts[2].getAttribute('type');

// // change styling
// // document.getElementById('task-title').style.color="forestgreen";

// //change content <tag> contet change </tag> 
// // document.getElementById('task-title').textContent="My List";//<tag> contet change </tag>
// // document.getElementById('task-title').innerText="My Task";
// // document.getElementById('task-title').innerHTML="My Jobs";

// // document.getElementById('task-title').innerHTML=`<span style="color : yellow">My Tasks</span>`;
// // document.getElementById('task-title').textContent="My List";// not ok use Html text
// // document.getElementById('task-title').innerText="<span style='color : yellow'>My Tasks</span>"; // not ok use Html text

// val = document.getElementsByClassName('list-group-item');//HTMLCollection
// val = document.getElementsByClassName('list-group-item')[0].style.color = "blue";
// // val=document.getElementById('task-title');// first element find by id
// val = document.getElementsByTagName('h4'); //HTMLCollection
// // val[0].style.color="red";

// //querySelector
// val = document.querySelector('#task-title');
// val = document.querySelector('.list-group-item');//get first element
// val = document.querySelector('h3');

// // querySelectorAll
// val = document.querySelectorAll('h3'); //NodeList
// val = document.querySelectorAll('.list-group-item');
// // val[0].style.color='forestgreen';
// val = document.querySelector('ul li:nth-child(odd)');
// val = document.querySelector('ul li:last-of-type').style.color = 'red';
// val = document.querySelector('ul');
// val = val.children;//HTMLCollection

// // let getli = document.querySelector('li');
// // val=getli.parentElement;
// // console.log(val);
// // let sb = getli.nextElementSibling;
// // console.log(sb);

// //create Element
// // const newli=document.createElement('li');
// // newli.className="list-group-item";
// // newli.id="list-group-item";

// //add attribute
// // newli.setAttribute('abc',"taz");
// // newli.textContent="Item 6";
// // newli.innerHTML=`Item 6 <a href="#" id="delete-item3" class="delet-item">
// // <ion-icon name="trash-outline"></ion-icon>
// // </a>`

// // newli.appendChild(document.createTextNode("Item 6"));
// // const newlink=document.createElement('a');
// // newlink.setAttribute('href',"#");
// // newlink.id='delete-item6';
// // newlink.classList.add('delet-item');
// // newlink.innerHTML=`<ion-icon name="trash-outline"></ion-icon>`;

// // newli.appendChild(newlink);
// // document.querySelector('ul.list-group').appendChild(newli);
// // console.log(newlink);
// // console.log(newli);

// //replace element
// // let createH2=document.createElement('h2');
// // createH2.id='task-title';
// // createH2.appendChild(document.createTextNode('My Jobs'));
// // document.getElementById('task-title').parentElement.replaceChild(createH2,document.getElementById('task-title'));
// // console.log(createH2);

// //remove element
// // let getalllis=document.querySelectorAll('li');
// // getalllis[3].remove();
// // console.log(getalllis);

// //classList.replace(old,new);

// //hasClass
// // val = getli.classList.contains('list-group-item');

// //attribute
// // val = getli.children;
// // let getfirstlink = val[0];
// // getfirstlink.setAttribute('href', 'https://www.google.com');
// // console.log(getfirstlink.getAttribute('href'));
// // console.log(getfirstlink.getAttribute('class'));

// // console.log(getfirstlink.hasAttribute('href'));



// // mouse event
// // var clearbtn = document.querySelector('#clearbtn');
// // let card = document.querySelector('.card');
// // const heading = document.querySelector('h3');

// //single click
// // clearbtn.addEventListener('click',getmouseventType);

// //doubleclick
// // clearbtn.addEventListener('dblclick',getmouseventType);

// //mousedown
// // clearbtn.addEventListener('mousedown',getmouseventType);

// //mouseup
// // clearbtn.addEventListener('mouseup',getmouseventType);

// //mouseeneter
// // card.addEventListener('mouseenter',getmouseventType);

// //mouseover
// // card.addEventListener('mouseover',getmouseventType);

// //mouseleave
// // card.addEventListener('mouseleave',getmouseventType);

// //mouseout
// // card.addEventListener('mouseout',getmouseventType);

// //mousemove
// // card.addEventListener('mousemove',getmouseventType);

// // let input = document.querySelector('#task');
// // const formel = document.getElementsByClassName('form');
// // const geth2 = document.querySelector('#task-title');

// //keydown
// // input.addEventListener('keydown',getinputeventType);

// //keypress 
// // input.addEventListener('keypress',getinputeventType);

// //keyup
// // input.addEventListener('keyup',getinputeventType);

// //focus
// // input.addEventListener('focus',getinputeventType);

// //blur
// // input.addEventListener('blur',getinputeventType);

// //cut
// // input.addEventListener('cut',getinputeventType);

// //paste
// // input.addEventListener('paste',getinputeventType);

// //input 
// // input.addEventListener('input',getinputeventType);

// // formel[0].addEventListener('submit',getinputeventType);

// // function getinputeventType(e){
// //     console.log('event Type : '+ e.type);
// //     console.log(e.target.value);
// //     geth2.textContent=e.target.value;
// //     // e.preventDefault();
// // }

// // function getmouseventType(e){
// //     console.log('event Type '+e.type);
// //     e.preventDefault();
// // }

// //event bubbling
// // document.querySelector('.task-title')
// //     .addEventListener('click', function () {
// //         console.log('i am working');
// //     });

// // document.querySelector('.card-content')
// //     .addEventListener('click', function () {
// //         console.log('i am working card-content');
// //     });
// // event delegation
// // const deleteItem = document.querySelector('.delet-item');
// // deleteItem.addEventListener('click', function (e) {
// //     console.log('i am working a tag');
// //     console.log(e.target);
// //     e.preventDefault();
// // });

// // document.body.addEventListener('click',eventdeleg);

// // function eventdeleg(e) {
// //     // console.log('i am body click');
// //     // console.log(e.target);

// //     // if(e.target.getAttribute('name') === "trash-outline"){
// //     //     console.log('hey i am trash can');
// //     // }

// //     // if(e.target.parentElement.className === "delet-item"){
// //     //    console.log(e.target.parentElement);
// //     // }

// //     // if(e.target.parentElement.classList.contains("delet-item")){
// //     //     console.log(e.target.parentElement);
// //     //     e.target.parentElement.parentElement.remove();
// //     //  }
// //     // e.preventDefault();
// // }

const formel=document.querySelector('.form');
let task=document.getElementById('task');
formel.addEventListener('submit',function(e){
    // localStorage.setItem('mytask',task.value); not ok
    let setitems;
    if (localStorage.getItem('mytask') === null) {
        setitems = [];
    } else {
        setitems = JSON.parse(localStorage.getItem('mytask'));
    }
    setitems.push(task.value);
    localStorage.setItem('mytask',JSON.stringify(setitems));

    let items = localStorage.getItem('mytask');
    let newli;
    JSON.parse(items).forEach(item => {
        newli= document.createElement('li');
        newli.classList.add('list-group-item');
        newli.appendChild(document.createTextNode(item.toUpperCase()));
        const newlink=document.createElement('a');
        newlink.setAttribute('href',"#");
        newlink.id='delete-item6';
        newlink.classList.add('delet-item');
        newlink.innerHTML=`<ion-icon name="trash-outline"></ion-icon>`;
        newli.appendChild(newlink);
    });
    document.querySelector('ul.list-group').appendChild(newli);
    console.log(newli);

    e.preventDefault();
});

// // let items=localStorage.getItem('mytask');
// // JSON.parse(items).forEach( item => {
// //     let newli=document.createElement('li');
// //     newli.appendChild(document.createTextNode(item));
// //     console.log(newli);
// // });

 





// // console.log(val);
