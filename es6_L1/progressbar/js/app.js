var progressBar=document.querySelector('.progress-bar');
var downloadBtn=document.querySelector('.downloadBtn');
var setUrl="https://www.google.com";

downloadBtn.addEventListener('click',function(){
    downloadBtn.setAttribute('disabled',true);
    var setwidth=0;
    var setInv=setInterval(progressInc,80);
    function progressInc () {
        if(setwidth >=100) {
            clearInterval(setInv);
            window.location.href=setUrl;
            setwidth=0;
        }
        else {
            setwidth ++;
            progressBar.style.width=`${setwidth}%`;

            progressBar.setAttribute('data-inc',`${setwidth}%`);
        }
    }
});