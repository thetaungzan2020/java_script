var progressbar=document.getElementById('progress-bar');

window.onscroll=function() {
    scrollPoint();
}

function scrollPoint () {
    // scrollTop *100 / (Project Hight - Current Height);
    var getScrollTop = document.documentElement.scrollTop;
    var getProjectHeight= document.documentElement.scrollHeight;
    var getScreenHeight= document.documentElement.clientHeight;

    var calcCliHeight= getProjectHeight-getScreenHeight;
    var getfinal= Math.round(getScrollTop *100 / calcCliHeight);

    progressbar.style.width= `${getfinal}%`;
}


function printMe() {
    window.print();
}