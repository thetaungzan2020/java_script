var accTitles = document.querySelectorAll('.acctitle');
var accContents=document.getElementsByClassName('accContent');

for (var x = 0; x < accTitles.length; x++) {
    accTitles[x].addEventListener('click', function () {
        this.classList.toggle('active');
        if (this.nextElementSibling.style.height) {
            this.nextElementSibling.style.height = null;
        }
        else {
            // console.log(this.nextElementSibling.scrollHeight);
            this.nextElementSibling.style.height = this.nextElementSibling.scrollHeight + "px";
        }
    });
    if(accTitles[x].classList.contains('active')){
        accContents[x].style.height=accContents[x].scrollHeight+'px';
    }
}


