const slides = document.getElementsByClassName('carousel-item');
var dots = document.querySelectorAll('.dots');
var currslide = 1;

document.getElementById('prev').addEventListener('click', function () {
    carousel(currslide -= 1);
});

document.getElementById('next').addEventListener('click', function () {
    carousel(currslide += 1);
});

carousel(currslide);

function carousel(slideNumber) {
    var x;
    for (x = 0; x < slides.length; x++) {
        slides[x].style.display = 'none';
    }

    dots.forEach(dot => {
        dot.classList.remove('active');
    })
    
    if (slideNumber > slides.length) {
        currslide = 1;
    }
    else if (slideNumber < 1) {
        currslide = slides.length;
    }
    slides[currslide - 1].style.display = "block";
    dots[currslide - 1].classList.add('active');
    // console.log(currslide);
}

dots.forEach((dot, idx) => {
    dot.addEventListener('click', function () {
        currslide = this.getAttribute('data-bs-slide-to');
        carousel(currslide);
    });
});
